import pyttsx
engine = pyttsx.init()
engine.setProperty('rate', 120)
with open('./feeds/greet.txt') as f:
    lines = f.read().splitlines()
engine.say(lines)
with open('./feeds/weather.txt') as f:
    lines = f.read().splitlines()
engine.say(lines)
with open('./feeds/news.txt') as f:
    lines = f.read().splitlines()
engine.say(lines)
with open('./feeds/tech.txt') as f:
    lines = f.read().splitlines()
engine.say(lines)
engine.say("thats all for now")
engine.runAndWait()