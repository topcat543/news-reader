newsfile = open("./feeds/weather.txt", "w")
from pprint import pprint
import requests
r = requests.get('http://api.wunderground.com/api/3401c3db105d1f6d/conditions/q/UK/Telford.json')
data = r.json()

newsfile.write("The weather in " + data['current_observation']['display_location']['city'] + " is, " + data['current_observation']['weather'])
newsfile.write("\r\n")
newsfile.write("The Current temperature is, " +  str(data['current_observation']["temp_c"]) + " degrees C and it feels like, " + str(data['current_observation']['feelslike_c']))
newsfile.write("\r\n")
newsfile.write("The wind is currently " + data['current_observation']["wind_string"])
newsfile.close()