import sys
reload(sys)
sys.setdefaultencoding('utf-8')
import cgi
import feedparser
import fileinput
import re
newsfile = open("./feeds/tech.txt", "w")
d = feedparser.parse('http://feeds.bbci.co.uk/news/technology/rss.xml?edition=uk')
newsfile.write("This is the latest tech headlines");
newsfile.write("\r\n")
newsfile.write("\r\n")
i = 0
while i < 4 :
	newsfile.write(d.entries[i]['title'] + ". \r\n" + d.entries[i]['summary'] + "\r\n")
	newsfile.write("\r\n")
	i = i+1
newsfile.close()