This my very first python project. IT reads the latest news stories from the BBC as well as the BBC's latest tech news.

It has only been tested on windows so far.

It uses pyttsx to read out the news headlines and summary got by feedparser.

## WINDOWS: ##
To use it, first make sure you have the required dependencies installed first:

* pyttsx

* pywin32

* feedparser


Then just run the 'readthenews.py' file and it should read you the latest news